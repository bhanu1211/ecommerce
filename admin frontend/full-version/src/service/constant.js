const service = {}

if (process.env.NODE_ENV === 'production') {
  service.API_URL = 'http://localhost:2000/'
} else {
  service.API_URL = 'http://localhost:2000/'
}

export default service
