import React, { useState } from 'react'
//import { useLocation } from 'react-router-dom'
import { Card, CardHeader, CardTitle, CardBody, Col, Input, Form, Button, Label, Row } from 'reactstrap'
import axios from 'axios'
import toast from 'react-hot-toast'

const AddCategory = () => {
  const [name, setName] = useState('')

  const Category = async event => {
    event.preventDefault()
    try {
      const response = await axios.post('http://localhost:2000/category/addcategory', {
        name
      })
      toast.success('category added Successfully.')
 
      console.log(response.data)
      // do something with the response, such as saving a token to local storage
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <Card>
      <CardHeader>
        <CardTitle tag='h4'>Category</CardTitle>
      </CardHeader>

      <CardBody>
        <Form>
          <Row className='mb-1'>
            <Label sm='3' for='name'>
              Category name
            </Label>
            <Col sm='9'>
              <Input type='text' id='name' onChange={e => setName(e.target.value)} />
            </Col>
          </Row>

          <Row>
            <Col className='d-flex' md={{ size: 9, offset: 3 }}>
              <Button className='me-1' color='primary' type='submit' onClick={Category}>
                Submit
              </Button>
              <Button outline color='secondary' type='reset'>
                Reset
              </Button>
            </Col>
          </Row>
        </Form>
      </CardBody>
    </Card>
  )
}

export default AddCategory
