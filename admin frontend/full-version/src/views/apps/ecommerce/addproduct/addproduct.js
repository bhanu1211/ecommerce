import toast from 'react-hot-toast'
import Select from 'react-select'
import classnames from 'classnames'
import { useForm, Controller } from 'react-hook-form'
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'
import axios from '../../../../service/axios'
// import service from '../../../../service/constant'
// ** Custom Components
import { selectThemeColors } from '@utils'

// ** Reactstrap Imports
import { Card, CardHeader, CardTitle, CardBody, Button, Label, Input, Form, Col, Row, FormFeedback } from 'reactstrap'
import { useEffect, useState } from 'react'
// import { useState } from 'react'

const BasicHookForm = () => {
  
  // const [data, setData] = useState(null)
  //validation
  const validationScheme = yup.object().shape({
    title: yup.string().required('title is required.'),
    price: yup.string().required('price is required.'),
    shortdescription: yup.string().required('Shortdescription is required.'),
    longdescription: yup.string().required('Longdescription is required.'),
    availblestock: yup.string().required('Availablestock is required.'),
    categoryName: yup.object().nullable().required('Category is a required.')
  })

  // ** Hooks
  const {
    reset,
    control,
    register,
    handleSubmit,
    formState: { errors }
  } = useForm({ resolver: yupResolver(validationScheme) })

  const onSubmit = rawData => {
    const temp = []
    const temp2 = []

    rawData.variationName.map(i => {
      temp.push(i.value)
    })
    rawData.variationOptionsName.map(i => {
      temp2.push(i.value)
    })

    console.log('rawData-----', rawData)
    const formData = new FormData()
    formData.append('title', rawData.title)
    formData.append('price', rawData.price)
    formData.append('shortdescription', rawData.shortdescription)
    formData.append('availblestock', rawData.availblestock)
    formData.append('longdescription', rawData.longdescription)
    formData.append('categoryId', rawData.categoryName.value)
    formData.append('variationId', JSON.stringify(temp))
    formData.append('variationoptionsId', JSON.stringify(temp2))
    formData.append('image', rawData.image[0])

    console.log('temp', temp)
    console.log('289292929--', rawData.variationOptionsName)
    const createProduct = async data => {
      return await axios.post('/product/', data, {
        headers: {
          'Content-Type': 'multipart/form-data; boundary=<calculated when request is sent>'
        }
      })
    }
    window.location.href.endsWith('add')
    createProduct(formData)
      .then(res => {
        toast.success('product added Successfully.')
        console.log(res)
      })
      .catch(err => {
        toast.error(err.response?.data?.message || 'Something went wrong!')
      })
  }

  // setData(data)

  const { ref: refTitle, ...restTitle } = register('title')
  const { ref: refPrice, ...restPrice } = register('price')
  const { ref: refAvailblestock, ...restAvailblestock } = register('availblestock')

  const { ref: refShortdescription, ...restShortdescription } = register('shortdescription')
  const { ref: refLongdescription, ...restLongdescription } = register('longdescription')
  const { ref: refImage, ...restImage } = register('image')

  const [categoryData, setCategoryData] = useState([])
  const [variationData, setVariationData] = useState([])
  const [variationoptionData, setVariationoptionData] = useState([])

  const getCategoryData = async () => {
    return await axios
      .get('/category/')
      .then(res => {
        const options = []
        res.data.map(row => {
          options.push({ value: row.id, label: row.name })
        })
        setCategoryData(options)
        console.log('r3r3rr53535-----', options)
      })
      .catch(err => {
        toast.error(err.response?.data?.message || 'data ni aa rha category se!')
      })
  }

  useEffect(() => {
    getCategoryData()
    if (!window.location.href.endsWith('add')) {
    }
  }, [])

  const getVariationData = async () => {
    return await axios
      .get('/variation/getallvariation')
      .then(res => {
        const options = []
        res.data.map(row => {
          options.push({ value: row.id, label: row.name })
        })
        setVariationData(options)
        console.log('r3r3rr53535-----', options)
      })
      .catch(err => {
        toast.error(err.response?.data?.message || 'data ni aa rha variation se!')
      })
  }

  useEffect(() => {
    getVariationData()
    if (!window.location.href.endsWith('add')) {
    }
  }, [])

  const getVariationoptionData = async () => {
    return await axios
      .get('/variationoption/getallvariationoption')
      .then(res => {
        const variation = []
        res.data.map(row => {
          variation.push({ value: row.id, label: row.name })
        })
        setVariationoptionData(variation)
        console.log('r3r3rr53535-----', variation)
      })
      .catch(err => {
        toast.error(err.response?.data?.message || 'data ni aa rha variationoption se!')
      })
  }

  useEffect(() => {
    getVariationoptionData()
    if (!window.location.href.endsWith('add')) {
    }
  }, [])

  const handleReset = () => {
    reset({
      emailBasic: '',
      firstNameBasic: '',
      lastNameBasic: '',
      ReactSelect: ''
    })
  }

  return (
    <Card>
      <CardHeader>
        <CardTitle tag='h4'>Add Product details</CardTitle>
      </CardHeader>
      <CardBody>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Row>
            <Col md='6' sm='12' className='mb-1'>
              <Label>Title</Label>
              <Input
                id='title'
                name='title'
                type='text'
                placeholder='Title'
                invalid={errors.title && true}
                {...restTitle}
                innerRef={refTitle}
              />
              {errors && errors.title && <FormFeedback>{errors.title.message}</FormFeedback>}
            </Col>
            <Col md='6' sm='12' className='mb-1'>
              <Label>Price</Label>
              <Input
                id='price'
                name='price'
                type='text'
                placeholder='Price'
                invalid={errors.price && true}
                {...restPrice}
                innerRef={refPrice}
              />
              {errors && errors.price && <FormFeedback>{errors.price.message}</FormFeedback>}
            </Col>
            <Col md='6' sm='12' className='mb-1'>
              <Label>Category Name</Label>
              <Controller
                id='categoryName'
                control={control}
                name='categoryName'
                render={({ field }) => (
                  <Select
                    isClearable
                    classNamePrefix='select'
                    placeholder={'Select Category'}
                    options={categoryData}
                    theme={selectThemeColors}
                    className={classnames('react-select', {
                      'is-invalid':
                        errors.categoryName?.message ||
                        errors.categoryName?.label.message ||
                        errors.categoryName?.value.message
                    })}
                    {...field}
                  />
                )}
              />

              {errors && errors.categoryName && (
                <FormFeedback>
                  {errors.categoryName?.message ||
                    errors.categoryName?.label.message ||
                    errors.categoryName?.value.message}
                </FormFeedback>
              )}
            </Col>
            <Col md='6' sm='12' className='mb-1'>
              <Label>Variation</Label>
              <Controller
                id='variationName'
                control={control}
                name='variationName'
                render={({ field }) => (
                  <Select
                    isClearable
                    isMulti
                    classNamePrefix='select'
                    placeholder={'Select variation'}
                    options={variationData}
                    theme={selectThemeColors}
                    className={classnames('react-select', {
                      'is-invalid':
                        errors.variationName?.message ||
                        errors.variationName?.label.message ||
                        errors.variationName?.value.message
                    })}
                    {...field}
                  />
                )}
              />

              {errors && errors.variationName && (
                <FormFeedback>
                  {errors.variationName?.message ||
                    errors.variationName?.label.message ||
                    errors.variationName?.value.message}
                </FormFeedback>
              )}
            </Col>
            <Col md='6' sm='12' className='mb-1'>
              <Label>Variation Options</Label>
              <Controller
                id='variationOptionsName'
                control={control}
                name='variationOptionsName'
                render={({ field }) => (
                  <Select
                    isClearable
                    isMulti
                    classNamePrefix='select'
                    placeholder={'Select variationoption'}
                    options={variationoptionData}
                    theme={selectThemeColors}
                    className={classnames('react-select', {
                      'is-invalid':
                        errors.variationOptionsName?.message ||
                        errors.variationOptionsName?.label.message ||
                        errors.variationOptionsName?.value.message
                    })}
                    {...field}
                  />
                )}
              />

              {errors && errors.variationOptionsName && (
                <FormFeedback>
                  {errors.variationOptionsName?.message ||
                    errors.variationOptionsName?.label.message ||
                    errors.variationOptionsName?.value.message}
                </FormFeedback>
              )}
            </Col>
            <Col md='6' sm='12' className='mb-1'>
              <Label>Availblestock</Label>
              <Input
                id='availblestock'
                name='availblestock'
                type='text'
                placeholder='Availblestock'
                invalid={errors.availblestock && true}
                {...restAvailblestock}
                innerRef={refAvailblestock}
              />
              {errors && errors.availblestock && <FormFeedback>{errors.availblestock.message}</FormFeedback>}
            </Col>
            <Col md='6' sm='12' className='mb-1'>
              <Label>Short Discription</Label>
              <Input
                id='shortDiscription'
                name='shortDiscription'
                type='textarea'
                placeholder='shortDiscription'
                invalid={errors.shortdescription && true}
                {...restShortdescription}
                innerRef={refShortdescription}
              />
              {errors && errors.shortdescription && <FormFeedback>{errors.shortdescription.message}</FormFeedback>}
            </Col>
            <Col md='6' sm='12' className='mb-1'>
              <Label>Long Discription</Label>
              <Input
                id='longDiscription'
                name='longDiscription'
                type='textarea'
                placeholder='longDiscription'
                invalid={errors.longdescription && true}
                {...restLongdescription}
                innerRef={refLongdescription}
              />
              {errors && errors.longdescription && <FormFeedback>{errors.longdescription.message}</FormFeedback>}
            </Col>

            <Col md='6' sm='12'>
              <Label className='form-label' for='inputFile'>
                Add image
              </Label>
              <Input
                type='file'
                id='inputFile'
                name='fileInput'
                accept='.jpg,.png,.jpeg'
                invalid={errors.image && true}
                {...restImage}
                innerRef={refImage}
              />
            </Col>
          </Row>

          <div className='d-flex mt-2'>
            <Button className='me-1' color='primary' type='submit'>
              Submit
            </Button>
            <Button outline color='secondary' type='reset' onClick={handleReset}>
              Reset
            </Button>
          </div>
        </Form>
      </CardBody>
    </Card>
  )
}

export default BasicHookForm
