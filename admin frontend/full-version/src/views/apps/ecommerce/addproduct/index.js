// ** React Imports
import { Fragment } from 'react'

// ** Product detail components

// ** Custom Components
import BreadCrumbs from '@components/breadcrumbs'

// ** Reactstrap Imports
import { Card, CardBody } from 'reactstrap'

// ** Store & Actions
// import { useSelector } from 'react-redux'

import '@styles/base/pages/app-ecommerce-details.scss'
import AddProduct from './addproduct'

const Details = () => {
  // ** Store Vars

  // const store = useSelector(state => state.ecommerce)

  return (
    <Fragment>
      <BreadCrumbs title='Product Details' data={[{ title: 'eCommerce' }, { title: 'Add Product' }]} />
      <div className='app-ecommerce-details'>
        <AddProduct></AddProduct>
      </div>
    </Fragment>
  )
}

export default Details
