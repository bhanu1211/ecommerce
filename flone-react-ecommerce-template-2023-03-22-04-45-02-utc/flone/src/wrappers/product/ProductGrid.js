import { Fragment, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
// import { getProducts } from "../../helpers/product";
import ProductGridSingle from '../../components/product/ProductGridSingle';
import axios from '../../services/axios-original';
// import service from '../../../src/services/constant';
const ProductGrid = ({ spaceBottomClass, category, type, limit }) => {
    // const { products } = useSelector((state) => state.product);
    const currency = useSelector((state) => state.currency);
    const { cartItems } = useSelector((state) => state.cart);
    const { wishlistItems } = useSelector((state) => state.wishlist);
  
    // const prods = getProducts(products, category, type, limit)

    useEffect(() => {
        getdata();
    }, []);

    const [product1, setProduct1] = useState([]);

    const getdata = async () => {
        await axios.get('product/').then((response) => {
            setProduct1(response.data);
            //  console.log(response.data);2
        });
    };

    return (
        <Fragment>
            {product1
                ?.slice(6, 10)

                ?.map((product) => {
                    return (
                        <div className="col-xl-3 col-md-6 col-lg-4 col-sm-6" key={product.id}>
                            <ProductGridSingle
                                spaceBottomClass={spaceBottomClass}
                                product={product}
                                currency={currency}
                                cartItem={cartItems.find((cartItem) => cartItem.id === product.id)}
                                wishlistItem={wishlistItems.find((wishlistItem) => wishlistItem.id === product.id)}
                                // compareItem={
                                //   compareItems.find(
                                //     (compareItem) => compareItem.id === product.id
                                //   )
                                // }
                            />
                        </div>
                    );
                })}
        </Fragment>
    );
};

ProductGrid.propTypes = {
    spaceBottomClass: PropTypes.string,
    category: PropTypes.string,
    type: PropTypes.string,
    limit: PropTypes.number,
};

export default ProductGrid;
