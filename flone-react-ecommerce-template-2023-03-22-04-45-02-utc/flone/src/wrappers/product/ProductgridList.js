import PropTypes from "prop-types";
import React, { Fragment, useState } from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import ProductGridListSingle from "../../components/product/ProductGridListSingle";
import axios from '../../services/axios-original';
const ProductGridList = ({
  products,
  spaceBottomClass
}) => {
  const currency = useSelector((state) => state.currency);
  const { cartItems } = useSelector((state) => state.cart);
  const { wishlistItems } = useSelector((state) => state.wishlist);
  // const { compareItems } = useSelector((state) => state.compare);
  
  useEffect(() => {
      getdata();
  }, []);

  const [product1, setProduct1] = useState([]);

  const getdata = async () => {
      await axios.get('product/').then((response) => {
          setProduct1(response.data);
          // console.log(response.data);
      });
  };


  
  return (
      <Fragment>
          {product1?.map((product) => {
              return (
                  <div className="col-xl-4 col-sm-6" key={product.id}>
                      <ProductGridListSingle
                          spaceBottomClass={spaceBottomClass}
                          product={product}
                          currency={currency}
                          cartItem={cartItems.find((cartItem) => cartItem.id === product.id)}
                          wishlistItem={wishlistItems.find((wishlistItem) => wishlistItem.id === product.id)}
                          
                      />
                  </div>
              );
          })}
      </Fragment>
  );
};

ProductGridList.propTypes = {
  products: PropTypes.array,
  spaceBottomClass: PropTypes.string,
};

export default ProductGridList;
