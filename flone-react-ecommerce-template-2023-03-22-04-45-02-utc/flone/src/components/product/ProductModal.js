import { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { EffectFade, Thumbs } from 'swiper';
import { Modal } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import Rating from './sub-components/ProductRating';

import { getProductCartQuantity } from '../../helpers/product';
import { addToCart } from '../../store/slices/cart-slice';
import { addToWishlist } from '../../store/slices/wishlist-slice';
import service from '../../../src/services/constant';
import { useParams } from 'react-router-dom';
import axios from '../../services/axios-original';

function ProductModal({ product, currency, discountedPrice, finalProductPrice, finalDiscountedPrice, show, onHide, wishlistItem }) {
    const [thumbsSwiper, setThumbsSwiper] = useState(null);
    const dispatch = useDispatch();
    const { cartItems } = useSelector((state) => state.cart);

    const [selectedProductColor, setSelectedProductColor] = useState();
    const [selectedProductSize, setSelectedProductSize] = useState();
    const [productStock, setProductStock] = useState(
        product.variation ? product.variation[0].size[0].availblestock : product.availblestock
    );
    const [quantityCount, setQuantityCount] = useState(1);
    const productCartQty = getProductCartQuantity(cartItems, product, selectedProductColor, selectedProductSize);

    const onCloseModal = () => {
        setThumbsSwiper(null);
        onHide();
    };

    useEffect(() => {
        // console.log(selectedProductSize);
        // console.log(selectedProductColor);
    }, [selectedProductSize, selectedProductColor]);
    return (
        <Modal show={show} onHide={onCloseModal} className="product-quickview-modal-wrapper">
            <Modal.Header closeButton></Modal.Header>

            <div className="modal-body">
                <div className="row">
                    <div className="col-md-5 col-sm-12 col-xs-12">
                        <div className="product-large-image-wrapper">
                            {/* <Swiper options={gallerySwiperParams}> */}
                            {/* {product.image &&
                                    product.image.map((img, i) => { */}
                            {/* return ( */}
                            {/* // <SwiperSlide key={i}> */}
                            <div className="single-image">
                                <img src={service.API_URL + product.image} className="img-fluid" alt="Product" />
                            </div>
                            {/* // </SwiperSlide> */}
                            {/* ); */}
                            {/* })} */}
                            {/* </Swiper> */}
                        </div>
                        {/* <div className="product-small-image-wrapper mt-15"> */}
                        {/* <Swiper options={thumbnailSwiperParams}> */}
                        {/* {product.image &&
                                    product.image.map((img, i) => {
                                        return ( */}
                        {/* // <SwiperSlide key={i}> */}
                        {/* <div className="single-image">
                                                    <img src={service.API_URL + product.image} className="img-fluid" alt="" />
                                                </div> */}
                        {/* // </SwiperSlide>
                                        );
                                    })} */}
                        {/* </Swiper> */}
                    </div>
                    {/* </div> */}
                    <div className="col-md-7 col-sm-12 col-xs-12">
                        <div className="product-details-content quickview-content">
                            <h2>{product.title}</h2>
                            <div className="product-details-price">
                                {discountedPrice !== null ? (
                                    <Fragment>
                                        <span>{currency.currencySymbol + finalDiscountedPrice}</span>{' '}
                                        <span className="old">{currency.currencySymbol + finalProductPrice}</span>
                                    </Fragment>
                                ) : (
                                    <span>{currency.currencySymbol + finalProductPrice} </span>
                                )}
                            </div>
                            {product.rating && product.rating > 0 ? (
                                <div className="pro-details-rating-wrap">
                                    <div className="pro-details-rating">
                                        <Rating ratingValue={product.rating} />
                                    </div>
                                </div>
                            ) : (
                                ''
                            )}
                            <div className="pro-details-list">
                                <p>{product.shortdescription}</p>
                            </div>

                            {product?.ProductVariations ? (
                                <div className="pro-details-size-color">
                                    <div className="pro-details-color-wrap">
                                        <span>Color</span>
                                        <div className="pro-details-color-content">
                                            {product.ProductVariations[0].Variationoption.map((single, key) => {
                                                return (
                                                    <label className={`pro-details-color-content--single ${single.name}`} key={key}>
                                                        {/* {console.log(single.name)} */}
                                                        <input
                                                            type="radio"
                                                            value={single.name}
                                                            name="product-color"
                                                            checked={single.name === selectedProductColor ? 'checked' : ''}
                                                            onChange={() => {
                                                                // console.log(single);
                                                                setSelectedProductColor(single.name);
                                                                setProductStock(product.availblestock);
                                                                setQuantityCount(1);
                                                            }}
                                                        />
                                                        <span className="checkmark"></span>
                                                    </label>
                                                );
                                            })}
                                        </div>
                                    </div>
                                    <div className="pro-details-size">
                                        <span>Size</span>
                                        <div className="pro-details-size-content">
                                            {/* {console.log(product.ProductVariations[1].Variation.Variationoptions)} */}
                                            {product.ProductVariations &&
                                                product.ProductVariations[1].Variationoption.map((item, key) => (
                                                    <label className={`pro-details-size-content--single ${item.name}`} key={key}>
                                                        {/* {console.log(single.name)} */}
                                                        <input
                                                            type="radio"
                                                            value={item.name}
                                                            checked={item.name === selectedProductSize ? 'checked' : ''}
                                                            onChange={() => {
                                                                setSelectedProductSize(item.name);
                                                                setProductStock(product.availblestock);
                                                                setQuantityCount(1);
                                                            }}
                                                        />
                                                        <span className="size-name">{item.name}</span>
                                                    </label>
                                                ))}
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                ''
                            )}
                            {product.affiliateLink ? (
                                <div className="pro-details-quality">
                                    <div className="pro-details-cart btn-hover">
                                        <a href={product.affiliateLink} rel="noopener noreferrer" target="_blank">
                                            Buy Now
                                        </a>
                                    </div>
                                </div>
                            ) : (
                                <div className="pro-details-quality">
                                    <div className="cart-plus-minus">
                                        <button
                                            onClick={() => setQuantityCount(quantityCount > 1 ? quantityCount - 1 : 1)}
                                            className="dec qtybutton"
                                        >
                                            -
                                        </button>
                                        <input className="cart-plus-minus-box" type="text" value={quantityCount} readOnly />
                                        <button
                                            onClick={() =>
                                                setQuantityCount(
                                                    quantityCount < productStock - productCartQty ? quantityCount + 1 : quantityCount
                                                )
                                            }
                                            className="inc qtybutton"
                                        >
                                            +
                                        </button>
                                    </div>
                                    <div className="pro-details-cart btn-hover">
                                        {productStock && productStock > 0 ? (
                                            <button
                                                onClick={() =>
                                                    dispatch(
                                                        addToCart({
                                                            ...product,
                                                            quantity: quantityCount,
                                                            selectedProductColor: selectedProductColor
                                                                ? selectedProductColor
                                                                : product.selectedProductColor
                                                                ? product.selectedProductColor
                                                                : null,
                                                            selectedProductSize: selectedProductSize
                                                                ? selectedProductSize
                                                                : product.selectedProductSize
                                                                ? product.selectedProductSize
                                                                : null,
                                                        })
                                                    )
                                                }
                                                disabled={productCartQty >= productStock}
                                            >
                                                {' '}
                                                Add To Cart{' '}
                                            </button>
                                        ) : (
                                            <button disabled>Out of Stock</button>
                                        )}
                                    </div>
                                    <div className="pro-details-wishlist">
                                        <button
                                            className={wishlistItem !== undefined ? 'active' : ''}
                                            disabled={wishlistItem !== undefined}
                                            title={wishlistItem !== undefined ? 'Added to wishlist' : 'Add to wishlist'}
                                            onClick={() => dispatch(addToWishlist(product))}
                                        >
                                            <i className="pe-7s-like" />
                                        </button>
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </Modal>
    );
}

ProductModal.propTypes = {
    currency: PropTypes.shape({}),
    discountedprice: PropTypes.number,
    finaldiscountedprice: PropTypes.number,
    finalproductprice: PropTypes.number,
    onHide: PropTypes.func,
    product: PropTypes.shape({}),
    show: PropTypes.bool,
    wishlistItem: PropTypes.shape({}),
};

export default ProductModal;
