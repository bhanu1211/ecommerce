import PropTypes from "prop-types";
import { useEffect, useState } from "react";

import { setActiveSort } from "../../helpers/product";
import axios from '../../services/axios-original';


const ShopCategories = ({ product,categories, getSortParams }) => {
  
   useEffect(() => {
       getdata();
   }, []);

   const [category1, setCategory1] = useState([]);

// console.log("---",category1[0].name)


   const getdata = async () => {
       await axios.get('/category/').then((response) => {
           setCategory1(response.data);
          });

        };
       
  return (
      <div className="sidebar-widget">
          <h4 className="pro-sidebar-title">Categories </h4>
          <div className="sidebar-widget-list mt-30">
              {categories ? (
                  <ul>
                      <li>
                          <div className="sidebar-widget-list-left">
                              <button
                                  onClick={(e) => {
                                      getSortParams('category', '');
                                      setActiveSort(e);
                                  }}

                              >
                                  <span className="checkmark" /> All Categories
                              </button>
                          </div>
                      </li>
                      {category1?.map((category, key) => {
                          return (
                              <li key={key}>
                                  <div className="sidebar-widget-list-left">
                                      <button
                                          onClick={(e) => {
                                              getSortParams('category', category);
                                              setActiveSort(e);
                                          }}
                                      >
                                          <span className="checkmark" /> {category.name}
                                      </button>
                                  </div>
                              </li>
                          );
                      })}
                  </ul>
              ) : (
                  'No categories found'
              )}
          </div>
      </div>
  );
};

ShopCategories.propTypes = {
  categories: PropTypes.array,
  getSortParams: PropTypes.func,
    product: PropTypes.shape({}),
};

export default ShopCategories;
