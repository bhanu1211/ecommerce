import PropTypes from 'prop-types';
import React, { Fragment, useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { getProductCartQuantity } from '../../helpers/product';

import { addToCart } from '../../store/slices/cart-slice';
import { addToWishlist } from '../../store/slices/wishlist-slice';

// import service from '../../../src/services/constant';
// import axios from '../../services/axios-original';

const ProductDescriptionInfo = ({
    product,
    discountedPrice,
    currency,
    finalDiscountedPrice,
    finalProductPrice,
    cartItems,
    wishlistItem,
    // compareItem,
}) => {
    const dispatch = useDispatch();
    const [selectedProductColor, setSelectedProductColor] = useState();
    const [selectedProductSize, setSelectedProductSize] = useState();
    const [productStock, setProductStock] = useState(
        product.variation ? product.variation[0].size[0].availblestock : product.availblestock
    );
    const [quantityCount, setQuantityCount] = useState(1);

    const productCartQty = getProductCartQuantity(cartItems, product, selectedProductColor, selectedProductSize);

    // const temp1 = [];
    // const temp2 = [];

    // product?.ProductVariations?.forEach((i) => {
    //     var newitem = i.Variationoption.filter((word) => !product.ProductVariation.include(word));

    //     if (newitem[0].variationId === 1) {
    //         temp1.push(newitem);
    //     } else {
    //         temp2.push(newitem);
    //     }
    // });
    // console.log('===', product);
    // console.log('===', temp2);

    useEffect(() => {
        // console.log(selectedProductSize);
        // console.log(selectedProductColor);
    }, [selectedProductSize, selectedProductColor]);

    const temp = [];
    temp.push(product.Category);
    // console.log('==', temp);

    return (
        <div className="product-details-content ml-70">
            <h2>{product.title}</h2>
            <div className="product-details-price">
                {discountedPrice !== null ? (
                    <Fragment>
                        <span>{currency.currencySymbol + finalDiscountedPrice}</span>{' '}
                        <span className="old">{currency.currencySymbol + finalProductPrice}</span>
                    </Fragment>
                ) : (
                    <span>{currency.currencySymbol + finalProductPrice} </span>
                )}
            </div>
            <div className="pro-details-list">
                <p>{product.shortdescription}</p>
            </div>

            {product?.ProductVariations ? (
                <div className="pro-details-size-color">
                    <div className="pro-details-color-wrap">
                        <span>Color</span>
                        <div className="pro-details-color-content">
                            {product.ProductVariations[0].Variationoption.map((single, key) => {
                                return (
                                    <label className={`pro-details-color-content--single ${single.name}`} key={key}>
                                        {/* {console.log(single.name)} */}
                                        <input
                                            type="radio"
                                            value={single.name}
                                            name="product-color"
                                            checked={single.name === selectedProductColor ? 'checked' : ''}
                                            onChange={() => {
                                                // console.log(single);
                                                setSelectedProductColor(single.name);
                                                setProductStock(product.availblestock);
                                                setQuantityCount(1);
                                            }}
                                        />
                                        <span className="checkmark"></span>
                                    </label>
                                );
                            })}
                        </div>
                    </div>
                    <div className="pro-details-size">
                        <span>Size</span>
                        <div className="pro-details-size-content">
                            {/* {console.log(product.ProductVariations[1].Variation.Variationoptions)} */}
                            {product.ProductVariations &&
                                product.ProductVariations[1].Variationoption.map((single) => (
                                    <label className={`pro-details-size-content--single`} key={single.name}>
                                        {/* {console.log(single.name)} */}
                                        <input
                                            type="radio"
                                            value={single.name}
                                            checked={single.name === selectedProductSize ? 'checked' : ''}
                                            onChange={() => {
                                                setSelectedProductSize(single.name);
                                                setProductStock(product.availblestock);
                                                setQuantityCount(1);
                                            }}
                                        />
                                        <span className="size-name">{single.name}</span>
                                    </label>
                                ))}
                        </div>
                    </div>
                </div>
            ) : (
                ''
            )}

            {product.affiliateLink ? (
                <div className="pro-details-quality">
                    <div className="pro-details-cart btn-hover ml-0">
                        <a href={product.affiliateLink} rel="noopener noreferrer" target="_blank">
                            Buy Now
                        </a>
                    </div>
                </div>
            ) : (
                <div className="pro-details-quality">
                    <div className="cart-plus-minus">
                        <button onClick={() => setQuantityCount(quantityCount > 1 ? quantityCount - 1 : 1)} className="dec qtybutton">
                            -
                        </button>
                        <input className="cart-plus-minus-box" type="text" value={quantityCount} readOnly />
                        <button
                            onClick={() =>
                                setQuantityCount(quantityCount < product.availblestock - productCartQty ? quantityCount + 1 : quantityCount)
                            }
                            className="inc qtybutton"
                        >
                            +
                        </button>
                    </div>
                    <div className="pro-details-cart btn-hover">
                        {product.availblestock && product.availblestock > 0 ? (
                            <button
                                onClick={() =>
                                    dispatch(
                                        addToCart({
                                            ...product,
                                            quantity: quantityCount,
                                            selectedProductColor: selectedProductColor
                                                ? selectedProductColor
                                                : product.selectedProductColor
                                                ? product.selectedProductColor
                                                : null,
                                            selectedProductSize: selectedProductSize
                                                ? selectedProductSize
                                                : product.selectedProductSize
                                                ? product.selectedProductSize
                                                : null,
                                        })
                                    )
                                }
                                disabled={productCartQty >= product.availblestock}
                            >
                                Add To Cart
                            </button>
                        ) : (
                            <button disabled>Out of Stock</button>
                        )}
                    </div>
                    <div className="pro-details-wishlist">
                        <button
                            className={wishlistItem !== undefined ? 'active' : ''}
                            disabled={wishlistItem !== undefined}
                            title={wishlistItem !== undefined ? 'Added to wishlist' : 'Add to wishlist'}
                            onClick={() => dispatch(addToWishlist(product))}
                        >
                            <i className="pe-7s-like" />
                        </button>
                    </div>
                </div>
            )}

            <div className="pro-details-meta">
                <span>Categories :</span>
                <ul>
                    {temp?.map((item) => {
                        // console.log("========",product)
                        return <div>{item?.name}</div>;
                    })}
                </ul>
            </div>

            {product.tag ? (
                <div className="pro-details-meta">
                    <span>Tags :</span>
                    <ul>
                        {product.tag.map((single, key) => {
                            return (
                                <li key={key}>
                                    <Link to={process.env.PUBLIC_URL + '/shop-grid-standard'}>{single}</Link>
                                </li>
                            );
                        })}
                    </ul>
                </div>
            ) : (
                ''
            )}
            <div className="pro-details-social">
                <ul>
                    <li>
                        <a href="//facebook.com">
                            <i className="fa fa-facebook" />
                        </a>
                    </li>
                    <li>
                        <a href="//dribbble.com">
                            <i className="fa fa-dribbble" />
                        </a>
                    </li>
                    <li>
                        <a href="//pinterest.com">
                            <i className="fa fa-pinterest-p" />
                        </a>
                    </li>
                    <li>
                        <a href="//twitter.com">
                            <i className="fa fa-twitter" />
                        </a>
                    </li>
                    <li>
                        <a href="//linkedin.com">
                            <i className="fa fa-linkedin" />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    );
};

ProductDescriptionInfo.propTypes = {
    cartItems: PropTypes.array,
    compareItem: PropTypes.shape({}),
    currency: PropTypes.shape({}),
    discountedPrice: PropTypes.number,
    finalDiscountedPrice: PropTypes.number,
    finalProductPrice: PropTypes.number,
    product: PropTypes.shape({}),
    wishlistItem: PropTypes.shape({}),
};

export default ProductDescriptionInfo;
