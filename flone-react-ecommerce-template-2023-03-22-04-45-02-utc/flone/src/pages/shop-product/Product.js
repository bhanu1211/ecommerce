import axios from '../../services/axios-original';
import React, { Fragment, useEffect, useState } from 'react';
// import { useSelector } from "react-redux";
import { useParams, useLocation } from 'react-router-dom';
import SEO from '../../components/seo';
import LayoutOne from '../../layouts/LayoutOne';
import Breadcrumb from '../../wrappers/breadcrumb/Breadcrumb';
// import RelatedProductSlider from "../../wrappers/product/RelatedProductSlider";
import ProductDescriptionTab from '../../wrappers/product/ProductDescriptionTab';
import ProductImageDescription from '../../wrappers/product/ProductImageDescription';
// import service from '../../../src/services/constant';

const Product = () => {
    let { pathname } = useLocation();
    const { id } = useParams();
    const [product1, setProduct1] = useState({});
    useEffect(() => {
        getDatabyId();
    }, []);
    const getDatabyId = async (data) => {
        const res = await axios.get('product/' + id, data).then((res) => {
            //  console.log('product1', res.data);
            const data = res.data.ProductVariations[0].Variationoption;
;
            //  console.log(data);
            
            setProduct1(res.data);
        });
    };

    // const { products } = useSelector((state) => state.product);
    // const product = products.find(product => product.id === id);

    return (
        <Fragment>
            <SEO titleTemplate="Product Page" description="Product Page of flone react minimalist eCommerce template." />

            <LayoutOne headerTop="visible">
                {/* breadcrumb */}
                <Breadcrumb
                    pages={[
                        { label: 'Home', path: process.env.PUBLIC_URL + '/' },
                        { label: 'Shop Product', path: process.env.PUBLIC_URL + pathname },
                    ]}
                />

                {/* product description with image */}
                <ProductImageDescription spaceTopClass="pt-100" spaceBottomClass="pb-100" product={product1} />

                {/* product description tab */}
                <ProductDescriptionTab spaceBottomClass="pb-90" productFullDesc={product1.longdescription} />

                {/* related product slider */}
                {/* <RelatedProductSlider
          spaceBottomClass="pb-95"
          category={product.category[0]}
        /> */}
            </LayoutOne>
        </Fragment>
    );
};

export default Product;
