const jwt = require('jsonwebtoken');
// const { roles } = require("../models");
const db = require('../db/models');
const User = db.User;
const Role = db.Role;
const Useradmin = db.Useradmin;

const auth = async (req, res, next) => {
    try {
        let token = req.headers.authorization;

        if (!token) {
            return res.status(401).json({ message: 'Unauthorized User2!' });
        }
        jwt.verify(token, process.env.JWT_SECRET_KEY, async function (err, decoded) {
            //console.log(decoded);
            if (err) {
                //console.error('err', err?.message);
                return res.status(401).json({ message: 'insert token' });
            }
            if (!decoded) {
                return res.status(401).json({ message: 'Unauthorized User2!' });
            }
            //console.log(decoded.details.id);
            const user = await Useradmin.findOne({
                where: {
                    id: decoded.user.id,
                },
            });
            const user1 = await User.findOne({
                where: {
                    id: decoded.user.id,
                },
            });
            //console.log(user);
            if (!user) {
                return res.status(404).send({
                    message: 'User not found vynynuns.',
                });
            }
            // if (!user1) {
            //     return res.status(404).send({
            //         message: 'User not found.',
            //     });
            // }
            // if (!user.status) {
            //     return res.status(400).send({
            //         message: 'Contact admin.',
            //     });
            // }
            req.user = user;
            req.user1 = user1;
            next();
        });
    } catch (error) {
        console.log(error);
        return res.status(401).json({ message: 'Unauthorized User2!' });
    }
};

module.exports = auth;
