
const variationController = require('./lib/controller');



module.exports = (app) => {
    // // get all category
    app.get(
        '/variation/getallvariation',

        variationController.getAllVariation
    );

    // // get Role by Id
    // app.get(
    //     '/api/v1/product/:id',
    //     // auth,
    //     // authPermission([modules.role, modules.role_edit, modules.user_add, modules.user_edit]),
    //     productController.getOneRole
    // );

    // create product
    app.post('/variation/addvariation', variationController.createVariation);
    

    // // update Role
    // app.put('/api/v1/product/:id', productController.updateRole);


    // // delete Role
    // app.delete('/api/v1/product/:id', productController.deleteRole);
};
