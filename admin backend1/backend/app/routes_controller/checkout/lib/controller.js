const db = require('../../../db/models');
const Checkout = db.Checkout;

const checkout = async (req, res) => {
    let details = {
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        country: req.body.country,
        street: req.body.street,
        city: req.body.city,
        state: req.body.state,
        zip: req.body.zip,
        phone: req.body.phone,
        //createdBy: req.user.id,
    };
    try {
        const chekout = await Checkout.create(details);
        return res.status(200).send({ chekout: chekout });
        console.log(chekout);
    } catch (error) {
        console.log(error);
        return res.status(401).json({ message: 'something went wrong' });
    }
};
module.exports = {
    checkout,
};
