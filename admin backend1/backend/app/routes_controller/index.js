module.exports = (app) => {
    require('./user')(app);
    require('./role')(app);
    require('./product')(app);
    require('./category')(app);
    require('./checkout')(app);
    require('./product_configure')(app);
    require('./product_variation')(app);
    require('./useradmin')(app);
    require('./variation')(app);
    require('./variation_option')(app);
};
