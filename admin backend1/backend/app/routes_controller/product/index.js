const productController = require('./lib/controller');

module.exports = (app) => {
    // get all product
    app.get('/product/', productController.getAllProducts);

    // get product by Id
    app.get('/product/:id', productController.getOneProduct);

    // create product
    app.post('/product/', productController.upload, productController.product);

    // update product
    app.put('/product/:id', productController.updateproduct);

    // delete product
    app.delete('/product/:id', productController.deleteproduct);
};
