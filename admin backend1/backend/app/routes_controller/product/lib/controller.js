const db = require('../../../db/models');
const Product = db.Product;
const path = require('path');
const ProductConfigure = db.ProductConfigure;
const ProductVariation = db.ProductVariation;
const Variationoption = db.Variationoption;
const Variation = db.Variation;

const Category = db.Category;
const multer = require('multer');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/images');
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + path.extname(file.originalname));
    },
});

const upload = multer({
    storage: storage,
    limits: { fileSize: '800000000000000000' },
    fileFilter: (req, file, cb) => {
        const fileTypes = /jpeg|jpg|png|gif/;
        const mimeType = fileTypes.test(file.mimetype);
        const extname = fileTypes.test(path.extname(file.originalname));

        if (mimeType && extname) {
            return cb(null, true);
        }
        cb('give proper file formate to upload');
    },
}).single('image');

const product = async (req, res) => {
    // console.log(res);

    const variationDetails = JSON.parse(req.body.variationId);
    const variationOptionDetails = JSON.parse(req.body.variationoptionsId);

    let details = {
        title: req.body.title,
        shortdescription: req.body.shortdescription,
        price: req.body.price,
        image: req?.file?.path,
        longdescription: req.body.longdescription,
        categoryId: req.body.categoryId,
        availblestock: req.body.availblestock,
    };

    const product = await Product.create(details);

    const allData = [];
    const allData2 = [];

    variationDetails.map((i) => {
        // console.log('mq1', i);
        allData.push({ variationId: i, productId: product.id });
    });
    // console.log('all', allData);

    variationOptionDetails.map((i) => {
        // console.log('mq2', i);
        allData2.push({ variationoptionsId: i, productId: product.id });
    });
    // console.log('all2', allData2);

    const productVariation = await ProductVariation.bulkCreate(allData);
    const productVariation2 = await ProductConfigure.bulkCreate(allData2);

    console.log('product---', productVariation);
    console.log('product---', productVariation2);

    return res.status(201).json({ data: product });
};

const getAllProducts = async (req, res) => {
    let products = await Product.findAll({
        where: {
            deletedAt: null,
        },
        attributes: {
            exclude: ['createdBy', 'updatedBy', 'deletedBy', 'updatedAt', 'createdAt', 'deletedAt'],
        },
        include: [
            { model: Category, attributes: ['name'] },

            {
                model: ProductVariation,
                attributes: ['id', 'variationId', 'productId'],
                include: [
                    {
                        model: Variation,
                    },
                ],
            },
            {
                model: ProductConfigure,
                attributes: ['id', 'variationoptionsId', 'productId'],
                include: [
                    {
                        model: Variationoption,
                        attributes: ['id', 'name', 'variationId'],
                    },
                ],
            },
        ],
    });

    const tempp = [];
    products = JSON.parse(JSON.stringify(products));
    products.forEach((i) => {
        // console.log('==================', i);

        i.ProductConfigures.forEach((jj) => {
            // console.log('---------', jj.Variationoption);
            tempp.push(jj.Variationoption);
            // console.log('===========', tempp);
        });
    });

    products.forEach((j) => {
        // console.log('-----', j.ProductVariations);
        j.ProductVariations.forEach((k) => {
            // console.log('------------', k);
            k.Variationoption = tempp.filter((m) => m.variationId === k.variationId);
        });
        //     // console.log('=====', j);
    });
    return res.status(200).send(products);
};

const getOneProduct = async (req, res) => {
    let id = req.params.id;
    let products = await Product.findOne({
        where: { id: id },
        include: [
            {
                model:Category,
                attributes:['name']
            },
            {
                model: ProductVariation,
                attributes: ['variationId'],
                include: [
                    {
                        model: Variation,
                        attributes: ['id', 'name'],
                    },
                ],
            },
            {
                model: ProductConfigure,
                attributes: ['variationoptionsId'],
                include: [
                    {
                        model: Variationoption,
                        attributes: ['id', 'name', 'variationId'],
                    },
                ],
            },
        ],
    });
    const newArray = [];
    const addArray = [];
    products = JSON.parse(JSON.stringify(products));
    newArray.push(products);

    newArray.forEach((i) => {
        i.ProductConfigures.forEach((l) => {
            addArray.push(l.Variationoption);
        });
    });

    newArray.forEach((i) => {
        i.ProductVariations = JSON.parse(JSON.stringify(i.ProductVariations));
        i.ProductVariations.forEach((l) => {
            l.Variationoption = addArray.filter((j) => j.variationId == l.variationId);
        });
    });
    return res.status(200).send(products);
};

// update user
const updateproduct = async (req, res) => {
    let id = req.params.id;
    const { title, shortdescription, price, availblestock, longdescription, image, categoryId } = req.body;
    const update = {
        title: title,
        shortdescription: shortdescription,
        availblestock: availblestock,
        price: price,
        longdescription: longdescription,
        image: image,
        categoryId: categoryId,
    };

    try {
        await Product.update(update, { where: { id: id } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        res.status(200).json({ message: 'not updated' });
    }
};

//5 delete user
const deleteproduct = async (req, res) => {
    const currentDate = new Date();
    const timestamp = currentDate.getTime();
    let id = req.params.id;
    const { title, shortdescription, price, availblestock, longdescription, image, categoryId } = req.body;
    const update = {
        title: title,
        shortdescription: shortdescription,
        availblestock: availblestock,
        price: price,
        longdescription: longdescription,
        image: image,
        categoryId: categoryId,
        // deletedBy: req.user.id,
        deletedAt: timestamp,
    };
    try {
        await Product.update(update, { where: { id: id, deletedAt: null } });

        return res.status(200).json({ message: 'deleted' });
    } catch (err) {
        console.log(err.message);
        res.status(200).json({ message: 'not deleted' });
    }
};

module.exports = {
    upload,
    product,
    getAllProducts,
    getOneProduct,
    updateproduct,
    deleteproduct,
};
