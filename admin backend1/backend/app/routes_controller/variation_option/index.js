
const variationoptionController = require('./lib/controller');



module.exports = (app) => {
    // // get all category
    app.get(
        '/variationoption/getallvariationoption',

        variationoptionController.getAllVariations
    );

    // // get Role by Id
    // app.get(
    //     '/api/v1/product/:id',
    //     // auth,
    //     // authPermission([modules.role, modules.role_edit, modules.user_add, modules.user_edit]),
    //     productController.getOneRole
    // );

    // create product
    app.post('/variationoption/addvariationoption', variationoptionController.createVariations);
    

    // // update Role
    // app.put('/api/v1/product/:id', productController.updateRole);


    // // delete Role
    // app.delete('/api/v1/product/:id', productController.deleteRole);
};
