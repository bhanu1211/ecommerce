const db = require('../../../db/models');

const Variationoption = db.Variationoption;

const createVariations = async (req, res) => {
    try {
        const { name, variationId } = req.body;

        const variation = await Variationoption.create({
            name: name,
            variationId: variationId,
        });
        return res.status(201).json({ data: variation });
    } catch (error) {
        console.log(error);
        return res.status(500).json({ message: 'something went wrong!' });
    }
};

// const updateVariation = async (req, res) => {
//     let id = req.params.id;

//     const { name } = req.body;
//     const update = {
//         name: name,
//     };

//     try {
//         await Variationoption.update(update, { where: { id: id } });
//         return res.status(200).send(update);
//     } catch (error) {
//         console.log(error);
//         return res.status(200).json({ message: 'something went wrong!' });
//     }
// };

// const deleteVariation = async (req, res) => {
//     let id = req.params.id;
//     const { name } = req.body;
//     const update = {
//         name: name,
//     };

//     try {
//         await Variationoption.update(update, { where: { id: id } });
//         res.status(200).send(update);
//     } catch (error) {
//         console.log(error);
//         return res.status(200).json({ message: 'something went wrong!' });
//     }
//     // await Role.destroy({where:{id:id,deletedby:null}})
//     // res.status(200).send('DELETED')
// };

// const getOneVariation = async (req, res) => {
//     let id = req.params.id;
//     let variation = await Variationoption.findOne({
//         where: { id: id },
//     });
//     return res.status(200).send(variation);
// };

const getAllVariations = async (req, res) => {
    const variations = await Variationoption.findAll({});
    return res.status(200).send(variations);
};

module.exports = {
    createVariations,
    //updateVariation,
    //deleteVariation,
    //getOneVariation,
    getAllVariations,
};
