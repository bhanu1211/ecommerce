
const useradminController = require('./lib/controller');



module.exports = (app) => {
    // // get all category
    app.get(
        '/useradmin/alluseradmin',

        useradminController.getAllUsers
    );

    // // get Role by Id
    app.get(
        '/useradmin/:id',
        // auth,
        // authPermission([modules.role, modules.role_edit, modules.user_add, modules.user_edit]),
        useradminController.getOneUser
    );

    // create product
    app.post('/useradmin/adduser', useradminController.adduser);
     app.post('/useradmin/', useradminController.login);
    

    // update Role
    app.put('/useradmin/:id', useradminController.updateUser);


    // delete Role
    app.delete('/useradmin/:id', useradminController.deleteUser);
};
