const db = require('../../../db/models');

const Category = db.Category;

const category = async (req, res) => {
    let details = {
        name: req.body.name,

        //createdBy: req.user.id,
    };
    try {
        const category = await Category.create(details);
        return res.status(200).send({ category: category });
        console.log(category);
    } catch (error) {
        console.log(error);
        return res.status(401).json({ message: 'something went wrong' });
    }
};
const getallcategory = async (req, res) => {
    const categorys = await Category.findAll({});
    console.log(categorys);
    res.status(200).send(categorys);
};
module.exports = {
    category,
    getallcategory,
};
