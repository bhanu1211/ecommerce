const db = require('../../../db/models');

const ProductConfigure = db.ProductConfigure;
const Variationoption=db.Variationoption

const getVariation_option = async (req, res) => {
    try {
        const option = await ProductConfigure.findAll({ where: { deletedAt: null } });
        res.status(200).send(option);
    } catch (err) {
        console.log('error in get all Variation_option', err);
    }
};
const getOneOption = async (req, res) => {
    try {
        let id = req.params.id;
        let option1 = await ProductConfigure.findAll({
            where: { productId: id },
            attributes: {
                exclude: ['createdBy', 'updatedBy', 'deletedBy', 'updatedAt', 'createdAt', 'deletedAt'],
            },
            include: [
                {
                    model: Variationoption,
                    attributes: ['id', 'name', 'variationId'],
                },
            ],
        });
        res.status(200).send(option1);
    } catch (err) {
        console.log('error', err);
    }
};
module.exports = {
    getVariation_option,
    getOneOption,
};
