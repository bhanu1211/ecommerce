// const auth = require('../../middlewares/middleware');
// const authPermission = require('../../middlewares/permission.middleware');
const roleController = require('./lib/controller');
// const { validate, validationRules } = require('./lib/validation');
// const { modules } = require('../../../utils');

module.exports = (app) => {
    // get all Role
    app.get(
        '/api/v1/role',
        // auth,
        // authPermission([modules.role, modules.role_edit, modules.user_add, modules.user_edit]),
        roleController.getAllRole
    );

    // get Role by Id
    app.get(
        '/api/v1/role/:id',
        // auth,
        // authPermission([modules.role, modules.role_edit, modules.user_add, modules.user_edit]),
        roleController.getOneRole
    );

    // create Role
    app.post('/api/v1/role', roleController.addRole);

    // update Role
    app.put('/api/v1/role/:id', roleController.updateRole);


    // delete Role
    app.delete('/api/v1/role/:id', roleController.deleteRole);
};
