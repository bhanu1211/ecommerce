const db = require('../../../db/models');

// const User = db.users
const Role = db.Role;
const User = db.User;
//main

//create role
const addRole = async (req, res) => {
    let info1 = {
        name: req.body.name,
        description: req.body.description,
        status: req.body.status,
        createdBy: req.user.id,
        //updatedBy:req.user.id
    };
    const role = await Role.create(info1);
    res.status(200).send(role);
    console.log(role);
};

//get all roles
const getAllRole = async (req, res) => {
    const roles = await Role.findAll({
        where: { deletedBy: null },
        include: [
            {
                model: User,
            },
        ],
    });
    res.status(200).send(roles);
};

//get one Role
const getOneRole = async (req, res) => {
    let id = req.params.id;
    let roles = await Role.findOne({ where: { id: id } });
    res.status(200).send(roles);
};

// update Role
const updateRole = async (req, res) => {
    let id = req.params.id;
    const { name, description, status } = req.body;
    const update = {
        name: name,
        description: description,
        status: status,
        updatedBy: req.user.id,
    };

    try {
        await Role.update(update, { where: { id: id, deletedby: null } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: 'not updated' });
    }
    // let roles = await Role.update(req.body , {where:{id:id,deletedby:null}})
    // res.status(200).send(roles)
};

//5 delete Role
const deleteRole = async (req, res) => {
    const currentDate = new Date();
    const timestamp = currentDate.getTime();
    let id = req.params.id;
    const { name, description, status } = req.body;
    const update = {
        name: name,
        description: description,
        status: status,
        deletedBy: req.user.id,
        deletedAt: timestamp,
    };

    try {
        await Role.update(update, { where: { id: id, deletedby: null } });
        return res.status(200).send(update);
    } catch (error) {
        console.log(error);
        return res.status(200).json({ message: 'not deleted' });
    }
};

module.exports = {
    addRole,
    getAllRole,
    getOneRole,
    updateRole,
    deleteRole,
};
