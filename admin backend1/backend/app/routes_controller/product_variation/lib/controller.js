const db = require('../../../db/models');

const ProductVariation = db.ProductVariation;

const craeteproductVariation = async (req, res) => {
    let details = {
        productId: req.body.productId,
        variationId: req.body.variationId,

        //createdBy: req.user.id,
    };
    try {
        const productvariation = await ProductVariation.create(details);
        return res.status(200).send({ data: productvariation });
        console.log(productvariation);
    } catch (error) {
        console.log(error);
        return res.status(401).json({ message: 'something went wrong' });
    }
};
const getallproductavariation = async (req, res) => {
    const productvariation = await ProductVariation.findAll({});
    console.log(productvariation);
    res.status(200).send({ data: productvariation });
};
module.exports = {
    craeteproductVariation,
    getallproductavariation,
};
