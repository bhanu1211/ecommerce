// const auth = require('../../middlewares/auth');
// const authPermission = require('../../middlewares/permission.middleware');
const userController = require('./lib/controller');
// const { modules } = require('../../../utils');
// const { validate, validationRules, loginRules, updateRules, updatePassword, updateProfileRules } = require('./lib/validation');

// const multer = require('multer');

// // allowed types for multer

// const allowedType = ['image/png', 'image/jpeg', 'image/jpg'];

// // diskStorage object for multer : contains path for storing image

// const fileStorage = multer.diskStorage({
//     destination: 'uploads/profile',

//     filename: (req, file, cb) => {
//         cb(null, Date.now() + '-' + file.originalname);
//     },
// });

// // cheking if uploaded file is allowed

// const fileFilter = (req, file, cb) => {
//     if (allowedType.includes(file.mimetype)) {
//         return cb(null, true);
//     } else {
//         req.fileValidationError = true;
//         return cb(null, false, req.fileValidationError);
//     }
// };

// // checking if multer throwed any error

// const multerMiddleware = (req, res, next) => {
//     if (req.fileValidationError) {
//         return res.status(400).send({ message: 'Only .png, .jpg and .jpeg format allowed!' });
//     }

//     next();
// };

// // multer upload object

// const uploads = multer({
//     storage: fileStorage,
//     fileFilter: fileFilter,
// });

module.exports = (app) => {
    // get all User
    app.get(
        '/user',
        // auth,
        // authPermission([modules.user]),
        userController.getAllUsers
    );

    // get User by Id
    app.get('/user/:id', userController.getOneUser);

    // create User
    app.post(
        '/user/register',
        // auth,
        // authPermission([modules.user_add]),
        // uploads.single('profilePic'),
        // multerMiddleware,
        // validationRules(),
        // validate,
        userController.register
    );

    // login
    app.post('/user/login', userController.login);

    // update User
    app.put(
        '/user/:id',
        // auth,
        // authPermission([modules.user_edit]),
        // uploads.single('profilePic'),
        // multerMiddleware,
        // updateRules(),
        // validate,
        userController.updateUser
    );

    

    // delete User
    app.delete('/user/:id', userController.deleteUser);
};
