// const { body, validationResult } = require('express-validator');
// const db = require('../../../db/models');
// const { Op } = require('sequelize');
// const User = db.User;

// const loginRules = () => {
//     return [
//         body('email').notEmpty().withMessage('Email is required').isEmail().withMessage('Enter valid email.'),
//         body('password').notEmpty().withMessage('Password is required'),
//     ];
// };

// const validationRules = () => {
//     return [
//         body('firstName').notEmpty().trim().withMessage('First Name is required.'),
//         body('lastName').notEmpty().trim().withMessage('Last Name is required.'),
//         body('mobile').notEmpty().trim().withMessage('Mobile is required.'),
//         body('email')
//             .trim()
//             .notEmpty()
//             .withMessage('Email is required.')
//             .isEmail()
//             .withMessage('Enter a valid email')
//             .custom(async (value) => {
//                 try {
//                     const user = await User.findOne({
//                         where: {
//                             email: value?.toLowerCase(),
//                         },
//                     });
//                     if (user) {
//                         return Promise.reject('Email already in use.');
//                     }
//                     return true;
//                 } catch (err) {
//                     return Promise.reject('Something went wrong');
//                 }
//             }),
//         body('password')
//             .notEmpty()
//             .withMessage('Password is required field')
//             .isLength({ min: 6 })
//             .withMessage('Minimum 6 characters is required'),
//         body('roleId').notEmpty().trim().withMessage('Role is required.'),
//     ];
// };

// const updatePassword = () => {
//     return [
//         body('oldPassword')
//             .notEmpty()
//             .withMessage('Old Password is required')
//             .isLength({ min: 6 })
//             .withMessage('Minimum 6 characters is required'),
//         body('newPassword')
//             .notEmpty()
//             .withMessage('New Password is required.')
//             .isLength({ min: 6 })
//             .withMessage('Minimum 6 characters is required'),
//         body('confirmPassword')
//             .notEmpty()
//             .withMessage('Confirm Password is required.')
//             .isLength({ min: 6 })
//             .withMessage('Minimum 6 characters is required'),
//     ];
// };

// const updateRules = () => {
//     return [
//         body('firstName').notEmpty().trim().withMessage('First Name is required.'),
//         body('lastName').notEmpty().trim().withMessage('Last Name is required.'),
//         body('mobile').notEmpty().trim().withMessage('Mobile is required.'),
//         body('email')
//             .notEmpty()
//             .withMessage('Email is required.')
//             .isEmail()
//             .withMessage('Enter a valid email')
//             .custom(async (value, { req }) => {
//                 try {
//                     const user = await User.findOne({
//                         where: {
//                             id: { [Op.ne]: req.params.id },
//                             email: value?.toLowerCase(),
//                         },
//                     });
//                     if (user) {
//                         return Promise.reject('Email already in use.');
//                     }
//                     return true;
//                 } catch (err) {
//                     console.log(err?.message);
//                     return Promise.reject('Something went wrong');
//                 }
//             }),
//         body('roleId').notEmpty().trim().withMessage('Role is required.'),
//     ];
// };

// const updateProfileRules = () => {
//     return [
//         body('firstName').notEmpty().trim().withMessage('First Name is required.'),
//         body('lastName').notEmpty().trim().withMessage('Last Name is required.'),
//         body('mobile').notEmpty().trim().withMessage('Mobile is required.'),
//         body('email').notEmpty().withMessage('Email is required.').isEmail().withMessage('Enter a valid email'),
//     ];
// };
// const validate = (req, res, next) => {
//     const errors = validationResult(req);

//     let errorSort = errors.array({
//         onlyFirstError: true,
//     });

//     if (!errors.isEmpty()) {
//         let error = errorSort[0];
//         return res.status(400).json({ message: error.msg, error: 'validation Error' });
//     }
//     next();
// };

// module.exports = {
//     validationRules,
//     loginRules,
//     validate,
//     updateRules,
//     updatePassword,
//     updateProfileRules,
// };
