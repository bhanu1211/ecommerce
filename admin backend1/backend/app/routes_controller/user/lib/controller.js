const Sequelize = require('sequelize');
// const Op = Sequelize.Op;

const db = require('../../../db/models');
const User = db.User;
// const Role = db.Role;
// const bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
// const status = require('../../../../utils/index').status;

//main
const login = (req, res) => {
    //const hashedPassword = await bcrypt.hash(password, 10);

    console.log('email', req.body.email);
    return User.findOne({
        where: {
            email: req.body.email,
        },
    }).then(async (result) => {
        if (!result) {
            return res.status(500).json({ message: 'something went wrong' });
        }
        // if (!bcrypt.compareSync(req.body.password, result.password)) {
        //     res.status(500).json({ message: 'something went wrong' });
        // }
        const payload = {
            user: {
                id: result.id,
                email: result.email,
                password: result.password.slice(-16),
            },
        };

        try {
            const token = jwt.sign(payload, process.env.JWT_SECRET_KEY);

            return res.send({
                message: 'login success',
                token: token,
            });
        } catch (error) {
            console.log(error);
            return res.status(401).json({ message: 'something went wrong' });
        }
    });
};

const register = async (req, res) => {
    let info = {
        email: req.body.email,
        username: req.body.username,
        password: req.body.password,
    };
    try {
        // const errors = validationResult(req);
        // if (!errors.isEmpty()) {
        //     return res.status(400).json({ errors: errors.array() });
        // }
        const user = await User.create(info);
        return res.status(200).send({ user: user });
        console.log(user);
    } catch (error) {
        console.log(error);
        return res.status(401).json({ message: 'something went wrong' });
    }
};

//create user

// const addUser = async (req, res) => {
//     let info = {
//         firstName: req.body.firstName,
//         lastName: req.body.lastName,
//         email: req.body.email,
//         roleId: req.body.roleId,
//         password: req.body.password,
//         status: req.body.status,
//         createdBy: req.user.id,
//     };
//     try {
//         //eror validation
//         //craete user
//         const errors = validationResult(req);
//         if (!errors.isEmpty()) {
//             res.status(400).json({ errors: errors.array() });
//         }
//         const user = await User.create(info);
//         res.status(200).send({ user: user });
//         console.log(user);
//     } catch (error) {
//         console.log(error);
//         res.status(500).json({ message: 'something went wrong' });
//     }
// };

//get all users
const getAllUsers = async (req, res) => {
    const users = await User.findAll({
        where: { deletedby: null },
      
    });
    res.status(200).send(users);
};

//get one user
const getOneUser = async (req, res) => {
    let id = req.params.id;
    let users = await User.findOne({ where: { id: id } });
    res.status(200).send(users);
};

// update user
const updateUser = async (req, res) => {
    let id = req.params.id;
    const { firstName, lastName, email,  password, status } = req.body;
    const update = {
        firstName: firstName,
        lastName: lastName,
        email: email,
       
        password: password,
        status: status,
        updatedBy: req.user.id,
    };
    // let users = await User.update(update, {where:{id:id}})
    // res.status(200).send(users)

    try {
        await User.update(update, { where: { id: id } });
        res.status(200).send(update);
    } catch (error) {
        console.log(error);
        res.status(200).json({ message: 'not updated' });
    }
};

//5 delete user
const deleteUser = async (req, res) => {
    const currentDate = new Date();
    const timestamp = currentDate.getTime();
   
    const { firstName, lastName, email,  password, status } = req.body;
    const update = {
        firstName: firstName,
        lastName: lastName,
        email: email,
        
        password: password,
        status: status,
        deletedBy: req.user.id,
        deletedAt: timestamp,
    };
};

module.exports = {
    login,
    register,
    //addUser,
    getAllUsers,
    getOneUser,
    updateUser,
    deleteUser,
};
