module.exports = (sequelize, DataTypes) => {
    const Category = sequelize.define(
        'Category',
        {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'category',
        }
    );

    Category.associate = (models) => {
        //role
        Category.hasMany(models.Product, {
            foreignKey: { name: 'categoryId', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        });

        Category.belongsTo(models.Useradmin, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            Category.belongsTo(models.Useradmin, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Category.belongsTo(models.Useradmin, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });
    };
    return Category;
};
