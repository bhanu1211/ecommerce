module.exports = (sequelize, DataTypes) => {
    const Variation = sequelize.define(
        'Variation',
        {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'variation',
        }
    );

    Variation.associate = (models) => {
        //

        Variation.belongsTo(models.Useradmin, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            Variation.belongsTo(models.Useradmin, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Variation.belongsTo(models.Useradmin, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });

        Variation.hasMany(models.Variationoption, {
            foreignKey: { name: 'variationId', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        });
        
        Variation.hasMany(models.ProductVariation, {
            foreignKey: { name: 'variationId', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        });
    };
    return Variation;
};
