module.exports = (sequelize, DataTypes) => {
    const Useradmin = sequelize.define(
        'Useradmin',
        {
            id: {
                type: DataTypes.UUID,
                allowNull: false,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
            },
            firstName: {
                type: DataTypes.STRING,
            },
            lastName: {
                type: DataTypes.STRING,
            },
            email: {
                type: DataTypes.STRING,
            },
            password: {
                type: DataTypes.STRING,
            },
            contact: {
                type: DataTypes.STRING,
            },
        },
        {
            tableName: 'useradmin',
        }
    );

    Useradmin.associate = (models) => {
        Useradmin.belongsTo(models.Role, {
            foreignKey: { name: 'roleId', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            Useradmin.belongsTo(Useradmin, {
                foreignKey: { name: 'createdBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Useradmin.belongsTo(Useradmin, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Useradmin.belongsTo(Useradmin, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });

        Useradmin.hasMany(models.Product, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            Useradmin.hasMany(models.Product, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Useradmin.hasMany(models.Product, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });

        Useradmin.hasMany(models.Variationoption, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            Useradmin.hasMany(models.Variationoption, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Useradmin.hasMany(models.Variationoption, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });

        Useradmin.hasMany(models.Variation, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            Useradmin.hasMany(models.Variation, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Useradmin.hasMany(models.Variation, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });

        Useradmin.hasMany(models.ProductConfigure, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            Useradmin.hasMany(models.ProductConfigure, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Useradmin.hasMany(models.ProductConfigure, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });

        Useradmin.hasMany(models.Category, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            Useradmin.hasMany(models.Category, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Useradmin.hasMany(models.Category, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });

        Useradmin.hasMany(models.Checkout, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            Useradmin.hasMany(models.Checkout, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Useradmin.hasMany(models.Checkout, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });
    };
    return Useradmin;
};
