module.exports = (sequelize, DataTypes) => {
    const Checkout = sequelize.define(
        'Checkout',
        {
            id: {
                type: DataTypes.UUID,
                allowNull: false,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
            },
            firstName: {
                type: DataTypes.STRING,
            },
            lastName: {
                type: DataTypes.STRING,
            },
            country: {
                type: DataTypes.STRING,
            },
            street: {
                type: DataTypes.STRING,
            },
            city: {
                type: DataTypes.STRING,
            },
            state: {
                type: DataTypes.STRING,
            },
            zip: {
                type: DataTypes.INTEGER,
            },
            phone: {
                type: DataTypes.INTEGER,
            },
            email: {
                type: DataTypes.STRING,
            },
        },
        {
            tableName: 'checkout',
        }
    );

    Checkout.associate = (models) => {
        //role
        Checkout.belongsTo(models.User, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
            as: 'cretedbyuser',
        });

 Checkout.belongsTo(models.Useradmin, {
     foreignKey: { name: 'createdBy', allowNull: true },
     onUpdate: 'CASCADE',
     onDelete: 'RESTRICT',
 }),
     Checkout.belongsTo(models.Useradmin, {
         foreignKey: { name: 'updatedBy', allowNull: true },
         onUpdate: 'CASCADE',
         onDelete: 'RESTRICT',
     }),
     Checkout.belongsTo(models.Useradmin, {
         foreignKey: { name: 'deletedBy', allowNull: true },
         onUpdate: 'CASCADE',
         onDelete: 'RESTRICT',
     });


    };
    return Checkout;
};
