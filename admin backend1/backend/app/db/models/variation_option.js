module.exports = (sequelize, DataTypes) => {
    const Variationoption = sequelize.define(
        'Variationoption',
        {
            id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'variationoption',
        }
    );

    Variationoption.associate = (models) => {
        //
        Variationoption.belongsTo(models.Variation, {
            foreignKey: { name: 'variationId', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        });

        Variationoption.belongsTo(models.Useradmin, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            Variationoption.belongsTo(models.Useradmin, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Variationoption.belongsTo(models.Useradmin, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });

        Variationoption.hasMany(models.ProductConfigure, {
            foreignKey: { name: 'variationoptionsId', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        });
    };
    return Variationoption;
};
