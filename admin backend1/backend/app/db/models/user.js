module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define(
        'User',
        {
            id: {
                type: DataTypes.UUID,
                allowNull: false,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
            },
            username: {
                type: DataTypes.STRING,
            },
            email: {
                type: DataTypes.STRING,
            },
            password: {
                type: DataTypes.STRING,
            },
        },
        {
            tableName: 'user',
        }
    );

    User.associate = (models) => {
       User.belongsTo(User, {
           foreignKey: { name: 'createdBy', allowNull: true },
           onUpdate: 'CASCADE',
           onDelete: 'RESTRICT',
       }),
           User.belongsTo(User, {
               foreignKey: { name: 'updatedBy', allowNull: true },
               onUpdate: 'CASCADE',
               onDelete: 'RESTRICT',
           }),
           User.belongsTo(User, {
               foreignKey: { name: 'deletedBy', allowNull: true },
               onUpdate: 'CASCADE',
               onDelete: 'RESTRICT',
           });

           };
    return User;
};
