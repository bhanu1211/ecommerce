module.exports = (sequelize, DataTypes) => {
    const Product = sequelize.define(
        'Product',
        {
            id: {
                type: DataTypes.UUID,
                allowNull: false,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
            },
            title: {
                type: DataTypes.STRING,
            },
            shortdescription: {
                type: DataTypes.STRING,
            },
            price: {
                type: DataTypes.INTEGER,
            },
            availblestock: {
                type: DataTypes.INTEGER,
            },
            image: {
                type: DataTypes.STRING,
            },
            longdescription: {
                type: DataTypes.STRING,
            },
            categoryId: {
                type: DataTypes.UUID,
                allowNull: true,
                references: {
                    model: 'Category',
                    key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            },

            createdAt: {
                allowNull: false,
                type: DataTypes.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: DataTypes.DATE,
                allowNull: false,
            },
            deletedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'product',
        }
    );

    Product.associate = (models) => {
        //role
        Product.belongsTo(models.Category, {
            foreignKey: { name: 'categoryId', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        });

        Product.belongsTo(models.Useradmin, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            Product.belongsTo(models.Useradmin, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            Product.belongsTo(models.Useradmin, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });

        Product.hasMany(models.ProductConfigure, {
            foreignKey: { name: 'productId', allowNull: true },
       
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        });
         Product.hasMany(models.ProductVariation, {
             foreignKey: { name: 'productId', allowNull: true },
             onUpdate: 'CASCADE',
             onDelete: 'RESTRICT',
         });

    };
    return Product;
};
