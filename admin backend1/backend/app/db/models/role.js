module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define(
        'Role',
        {
            id: {
                type: DataTypes.UUID,
                allowNull: false,
                defaultValue: DataTypes.UUIDV4,
                primaryKey: true,
            },
            name: {
                type: DataTypes.STRING,
            },
            description: {
                type: DataTypes.STRING,
            },
            status: {
                type: DataTypes.BOOLEAN,
            },
        },
        {
            tableName: 'role',
        }
    );
    Role.associate = (models) => {
        //HASMNAY
        Role.hasMany(models.Useradmin, {
            foreignKey: { name: 'roleId', allowNull: false },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        });
    };

    return Role;
};
