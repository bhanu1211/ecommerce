module.exports = (sequelize, DataTypes) => {
    const ProductConfigure = sequelize.define(
        'ProductConfigure',
        {
            id: {
                type: DataTypes.INTEGER,
                autoIncrement: true,
                allowNull: false,
                primaryKey: true,
            },
            createdAt: {
                type: DataTypes.DATE,
            },
            updatedAt: {
                type: DataTypes.DATE,
            },
        },
        {
            tableName: 'productconfigure',
        }
    );

    ProductConfigure.associate = (models) => {
        //
        ProductConfigure.belongsTo(models.Product, {
            foreignKey: { name: 'productId', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        });
        ProductConfigure.belongsTo(models.Variationoption, {
            foreignKey: { name: 'variationoptionsId', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        });
        ProductConfigure.belongsTo(models.Useradmin, {
            foreignKey: { name: 'createdBy', allowNull: true },
            onUpdate: 'CASCADE',
            onDelete: 'RESTRICT',
        }),
            ProductConfigure.belongsTo(models.Useradmin, {
                foreignKey: { name: 'updatedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            }),
            ProductConfigure.belongsTo(models.Useradmin, {
                foreignKey: { name: 'deletedBy', allowNull: true },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            });
    };
    return ProductConfigure;
};
