'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
    async up(queryInterface, Sequelize) {
        await queryInterface.createTable('Product', {
            id: {
                type: Sequelize.UUID,
                defaultValue: Sequelize.UUIDV4,
                allowNull: false,
                primaryKey: true,
            },
            title: {
                type: Sequelize.STRING,
            },
            shortdescription: {
                type: Sequelize.STRING,
            },
            price: {
                type: Sequelize.INTEGER,
            },
            availblestock:{
                type:Sequelize.INTEGER
            },
            image: {
                type: Sequelize.STRING,
            },
            longdescription: {
                type: Sequelize.STRING,
            },
            categoryId: {
                type: Sequelize.INTEGER,
                allowNull: true,
                references: {
                    model: 'Category',
                    key: 'id',
                },
                onUpdate: 'CASCADE',
                onDelete: 'RESTRICT',
            },

            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
                allowNull: false,
            },
            deletedAt: {
                type: Sequelize.DATE,
            },
            createdBy: {
                allowNull: true,
                type: Sequelize.UUID,
                references: {
                    model: 'Useradmin',
                    key: 'id',
                    onUpdate: 'CASCADE',
                    onDelete: 'RESTRICT',
                },
            },
            updatedBy: {
                allowNull: true,
                type: Sequelize.UUID,
                references: {
                    model: 'Useradmin',
                    key: 'id',
                    onUpdate: 'CASCADE',
                    onDelete: 'RESTRICT',
                },
            },
            deletedBy: {
                allowNull: true,
                type: Sequelize.UUID,
                references: {
                    model: 'Useradmin',
                    key: 'id',
                    onUpdate: 'CASCADE',
                    onDelete: 'RESTRICT',
                },
            },
        });
    },
    async down(queryInterface, Sequelize) {
        await queryInterface.dropTable('Product');
    },
};
