const { Op } = require('sequelize');
const db = require('../../app/db/models');
const Module = db.Module;
const Permission = db.Permission;

module.exports = {
    async addDaysSetHours(days, date = new Date(), hours = null, minutes = null, seconds = null) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        if (hours != null) result.setHours(hours);
        if (minutes != null) result.setHours(hours, minutes);
        if (seconds != null) result.setHours(hours, minutes, seconds);
        return result;
    },

    async getDate() {
        let d = new Date();
        d.setHours(d.getHours() + 5);
        d.setMinutes(d.getMinutes() + 30);
        return d;
    },

    async getPermissionByToken(user) {
        return new Promise(async (resolve, reject) => {
            try {
                let permissions = [];
                if (user?.Role?.isSystemAdmin) {
                    permissions = await Module.findAll({
                        attributes: [['id', 'moduleId']],
                        raw: true,
                    });
                } else {
                    permissions = await Permission.findAll({
                        attributes: ['moduleId'],
                        where: {
                            roleId: user.roleId,
                        },
                        raw: true,
                    });
                }
                const moduleIds = permissions.map((i) => i.moduleId);
                resolve(moduleIds);
            } catch (err) {
                reject(err);
            }
        });
    },
};
