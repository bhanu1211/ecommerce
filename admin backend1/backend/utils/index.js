module.exports = {
    modules: require('./lib/module'),
    messages: require('./lib/messages/api.response').messages,
    status: require('./lib/messages/api.response').status,
    sendGrid: require('./lib/sendGrid').sendMail,
    mailTemplate: require('./lib/sendGrid').mailTemplate,
    removeImage: require('./lib/removeImage').removeImage,
    rpInstance: require('./lib/razorpay').instance,
    common: require('./lib/common-function'),
};
