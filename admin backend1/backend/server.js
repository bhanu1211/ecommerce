require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');

const cors = require('cors');
const app = express();
var corOption = {
    origin: '*',
};

//middlewear
app.use(cors(corOption));
app.use(compression());
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
const db = require('./app/db/models');
app.use(bodyParser.json({ limit: '50mb' }));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/uploads', express.static('uploads'));

require('./app/routes_controller')(app);



//testing api
app.get('/', (req, res) => {
    res.json({ message: 'hello bhanu' });
});

//port
const PORT = process.env.PORT || 2000;

//server
app.listen(PORT, () => {
    console.log(`server is running on ${PORT}`);
});
